function menu_open() {
    const menu = document.getElementsByClassName('menu')[0];
    menu.animate([{opacity: '0'}, {opacity: '1'}], 500)
    menu.style.display = 'block'
}

function menu_close() {
    const menu = document.getElementsByClassName('menu')[0];
    menu.animate([{opacity: '1'}, {opacity: '0'}], 500)
    setTimeout(()=>{menu.style.display = 'none'}, 480)
}

function add_js_animation() {
    var element = document.querySelectorAll('section:not(#first) *, footer');

    for(var i=0;i<element.length;i++) {
        element[i].classList.add('js-animation')
    }
}

function showElementAnimation() {

    var element = document.getElementsByClassName('js-animation');
    if(!element) return;

    var showTiming = window.innerHeight > 768 ? 200 : 40;
    var scrollY = window.pageYOffset;
    var windowH = window.innerHeight;

    for(var i=0;i<element.length;i++) {
        var elemClientRect = element[i].getBoundingClientRect();
        var elemY = scrollY + elemClientRect.top;
        if(scrollY + windowH - showTiming > elemY) {
        element[i].classList.add('is-show');
    }
    }
}

add_js_animation();
showElementAnimation();
window.addEventListener('scroll', showElementAnimation);
