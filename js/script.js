const menu_open = () => {
    document.getElementsByClassName('menu')[0].classList.toggle('active')
}

const add_active = (wrapper) => {
    const codes = document.querySelectorAll([wrapper,'.source-code > div'].join(" "))
    const selections = document.querySelectorAll([wrapper,'.source-selection > a'].join(" "))
    const target = event.target
    codes.forEach((code)=> {
        code.classList.remove('active')
    })

    selections.forEach((selection)=> {
        selection.classList.remove('active')
    })

    target.classList.add('active')
    document.querySelector(target.getAttribute('target')).classList.add('active')
}

if(!navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)){
    const cursor = document.getElementById('cursor');

    document.addEventListener('mousemove',  (e) => {
        cursor.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
    });

    const linkElem = document.querySelectorAll('a');
    for (let i = 0; i < linkElem.length; i++) {
        linkElem[i].addEventListener('mouseover', (e) => {
            cursor.classList.add('hover');
        });
        linkElem[i].addEventListener('mouseout', (e) => {
            cursor.classList.remove('hover');
        });
    }
}